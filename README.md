# Furashu

> A Vue.js project to help you learn Japanese words...
> [Website](https://furasshu7.firebaseapp.com)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```