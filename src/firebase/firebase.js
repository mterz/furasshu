import * as firebase from 'firebase';

// Initialize Firebase
const config = {
  apiKey: "AIzaSyDl_5SJOl9p8zrKUpCZRAqrj-uBp26mnrk",
  authDomain: "furasshu7.firebaseapp.com",
  databaseURL: "https://furasshu7.firebaseio.com",
  projectId: "furasshu7",
  storageBucket: "furasshu7.appspot.com",
  messagingSenderId: "720121311215"
};
firebase.initializeApp(config);

const database = firebase.database();

export { firebase, database as default };

/*
Data:

publicCards: {
  '123das23sd': {
    kanjiMessage: o(kanji for kane),
    frontMessage: 'okane',
    backMessage: 'money',
    type: 1,
    tags: '[kanji,N5,days of the week]',
    byUser: 'JoeJapanese'
  },
  'sdasfe233': {
    kanjiMessage: (kanjiforbig)kii,
    frontMessage: ookii,
    backMessage: big,
    type 1,
    tags: '[N5, adjective, i-adjective]',
    byUser: 'aww'
  }
},
users: {
  '1233333' {
    name: 'JoeJapanese',
    options: {
      theme: 'light',
      difficulty: 'kanji, furigana'
    }
    privateCards: {
      'asdadadddd': {
        kanjiMessage: (tomo)(dachi),
        frontMessage: tomodachi,
        backMessage: friend,
        type: 1,
        tags: '[private, N5, noun]'
      }
    }
  }
}
*/

// database.ref('notes').once('value').then((snapshot) => {
//   const notes = [];

//   snapshot.forEach(childSnapshot => {
//     notes.push({
//       id: childSnapshot.key,
//       ...childSnapshot.val()
//     })
//   });

//   console.log(notes)
// })


// database.ref('notes/-LAo5SOxEUfjh8mp31GD').update({
//   body: 'Buy food'
// })


// database.ref().set({
//   name: 'Martin Terzijski',
//   age: 22,
//   isSingle: true,
//   location: {
//     city: 'Sofia',
//     country: 'Bulgaria'
//   },
//   stresslevel: 8,
//   job: 'software developer'
// }).then(() => {
//   console.log('Data is saved.')
// }).catch((err) => {
//   console.error('This failed.')
// });

// database.ref('isSingle').remove().then(() => {
//   console.log('Data removed!')
// }).catch((e) => {
//   console.error('Error removing data');
// })

// database.ref().update({
//   name: 'Mike',
//   age: 34,
//   job: 'Software developer',
//   isSingle: null
// })

// database.ref()
//   .on('value')
//   .then((snapshot) => {
//     const val = snapshot.val();
//     console.log(val);
//   })
//   .catch(e => {
//     console.error(e)
//   })

// const firebaseNotes = {
//   notes: {
//     sadasdasff: {
//       title: '',
//       body: ''
//     }
//   }
// }

// const notes = [{
//   id: '12',
//   title: '',
//   body: 'This is my note'
// }, {
//   id: '1323',
//   title: 'Another note',
//   body: 'sth'
// }];

// database.ref('notes').push({
//   title: 'Course topics',
//   body: 'Sthing else'
// });