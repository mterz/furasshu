import Dashboard from './components/routes/Dashboard.vue';
import AddCard from './components/routes/AddCard.vue';
import Options from './components/routes/Options.vue';
import EditCard from './components/routes/EditCard.vue';
import Test from './components/routes/Test.vue';

export default [
    {
        path: '/',
        component: Dashboard
    },
    {
        path: '/addCard',
        component: AddCard
    },
    {
        path: '/options',
        component: Options
    },
    {
        path: '/edit/:id',
        component: EditCard
    },
    {
        path: '/test',
        component: Test
    }
]