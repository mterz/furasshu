import Vue from 'vue';
import Vuex from 'vuex';

import database from '../firebase/firebase';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    cards: [
    ],
    options: {
      difficulty: 'medium'
    },
    searchValue: ""
  },
  mutations: {
    addCard(state, card) {
      state.cards.push(card);
    },
    updateCard(state, card) {
      const ind = state.cards.findIndex(c => c.id === card.id);
      state.cards.splice(ind, 1, card);
    },
    deleteCard(state, {id: id}) {
      const ind = state.cards.findIndex(c => c.id === id);
      state.cards.splice(ind, 1);
    },
    setCards(state, cards) {
      state.cards = cards;
      state.options.difficulty = localStorage.getItem('difficulty') || 'ultra';
    },
    setSearchValue(state, value) {
      state.searchValue = value;
    }
  },
  actions: {
    addCard: (context, card) => {
      database.ref('cards').push({
        ...card,
        byUser: 'admin'
      }).then((ref) => {
        let [frontMessage1, frontMessage2] = card.frontMessage.split('|');
        let [backMessage1, backMessage2] = card.backMessage.split('|');

        if (frontMessage2 === undefined) {
          frontMessage2 = null;
        }
        if (backMessage2 === undefined) {
          backMessage2 = null;
        }


        const tags = JSON.parse(card.tags);

        context.commit('addCard', {
          id: ref.key,
          frontMessage1,
          frontMessage2,
          backMessage1,
          backMessage2,
          tags,
          byUser: 'admin',
          front: true
        });
      }).catch((e) => {
        console.log('Could not push card to the database', e);
      })
    },
    updateCard: (context, {id, card}) => {
      database.ref(`cards/${id}`).update(card).then((ref) => {
        let [frontMessage1, frontMessage2] = card.frontMessage.split('|');
        let [backMessage1, backMessage2] = card.backMessage.split('|');

        if (frontMessage2 === undefined) {
          frontMessage2 = null;
        }
        if (backMessage2 === undefined) {
          backMessage2 = null;
        }
        
        const tags = JSON.parse(card.tags);

        context.commit('updateCard', {
          id,
          frontMessage1,
          frontMessage2,
          backMessage1,
          backMessage2,
          tags,
          byUser: 'admin',
          front: true
        })
      }).catch((e) => {
          console.log('Could not update card');
      })
    },
    deleteCard: (context, {id: id}) => {
      database.ref(`cards/${id}`).remove().then(() => {
        context.commit('deleteCard', {id: id})
      }).catch((e) => {
        console.log('Could not remove card');
      })
    },
    setCards: (context) => {
      database.ref('cards').once('value').then(snapshot => {
        const cards = [];

        snapshot.forEach(childSnapshot => {
          const card = childSnapshot.val();
          const [frontMessage1, frontMessage2] = card.frontMessage.split('|');
          const [backMessage1, backMessage2] = card.backMessage.split('|');
          const tags = JSON.parse(card.tags);
          cards.push({
            id: childSnapshot.key,
            frontMessage1,
            frontMessage2,
            backMessage1,
            backMessage2,
            tags,
            byUser: card.byUser,
            front: true
          })
        });
        context.commit('setCards', cards);
      }).catch(e => {
        console.log('Could not get cards from the database', e);
      })
    },
    setSearchValue: (context, searchValue) => {
      // Don't need to save the searchValue to the database
      context.commit('setSearchValue', searchValue.toLowerCase())
    }
  }
})