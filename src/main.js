import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import { store } from './store/store'

import router from './router';


import './firebase/firebase';

Vue.use(VueRouter);

new Vue({
  store: store,
  el: '#app',
  render: h => h(App),
  router
})
